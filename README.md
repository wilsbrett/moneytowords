# MoneyToWords
>This is a java class that converts 'cents' to words. For example "$12.04" becomes "TWELVE  DOLLARS AND FOUR CENTS".
>Requires String input and int currency.

### String input info:
>Removes and ignores all non numeric characters (including currency symbols like '$').
>Input must be in cents/pennies/pence.


### int currency info:
```
0 = Dollars & cents
1 = Pounds & pence
2 = Euros & cents 
```

### Examples:
```
MoneyToWords.convert("2134",0);
//Returns "TWENTY ONE DOLLARS AND THIRTY FOUR CENTS"

MoneyToWords.convert("$1001",1);
//Returns "TEN POUNDS AND ONE PENCE"

MoneyToWords.convert("4.01",2);
//Returns "FOUR EUROS AND ONE CENT"
```
