
/**
 * @author smleimberg
 */
public class MoneyToWords {
    
    private static String[] levels = {"","thousand","million","billion","trillion","quadrillion","quintillion","sextillion","septillion","octillion","nonillion","decillion","undecillion","duodecillion","tredecillion","quattuordecillion","quindecillion","sexdecillion","septendecillion","octodecillion","novemdecillion"};
    private static String[] nums = {"","one","two","three","four","five","six","seven","eight","nine"};
    private static String[] tens = {"","ten","twenty","thirty","fourty","fifty","sixty","seventy","eighty","ninety"};
    private static String[] teens = {"ten","eleven","twelve","thirteen","fourteen","fifteen","sixteen","seventeen","eighteen","nineteen"};
    private static String[] bills = {"dollars","pounds","euros"};
    private static String[] bill = {"dollar","pound","euro"};
    private static String[] coins = {"cents","pence","cents"};
    private static String[] coin = {"cent","pence","cent"};
    private static boolean billPlural = true;
    private static boolean coinPlural = true;
    
    /**
     * Converts "cents" to readable words. Detects if first character is $, £, or € and prints output accordingly. Default output is dollars($) & cents(¢). Removes all non digits from "cents" thus it ignores decimals, commas and any extra input. Max output is nine hundred ninety nine Novemcecillion (10^62). Must supply "cent" values, even if 00. Can only support hudredths of a cent "0.00" (no more no less). 
     * @param cents - the amount in hundredths of a currency to be converted to words: "£14.20" or "€200" or "14".
     * @return string - cents converted to words: "FOURTEEN POUND(S) AND TWENTY PENCE" or "TWO EURO(S)" or "FOURTEEN CENT(S)".
     * 
     */
    public static String convert(String cents,int currency){
        
        String billAmmount="";
        String coinAmmount="";
        String ammount="";
        cents = cents.replaceAll("[^0-9]", "");
        cents = removeLeadingZeros(cents);
        cents = reverse(cents);
        int inLength = cents.length();
        
        if(cents.isEmpty() || cents.matches("")){ //if empty return zero
            return "zero "+bill(currency)+" and zero "+coin(currency);
        }
        if( (inLength==1 && Integer.parseInt(""+cents.charAt(0))==1) || 
            (inLength>1 && Integer.parseInt(""+reverse(cents.substring(0, 2)))==1)){ //if one coin, coin is singular
            coinPlural=false;
        }
        if(inLength==3 && cents.charAt(2) =='1'){ //if one bill, bill is singular
            billPlural=false;
        }
        if(inLength>=3){ //if bills exist, generate words
            billAmmount = numString(cents.substring(2, inLength).toCharArray()) +" "+bill(currency);
        }
        if(inLength>=2){ //if coins exist, generate words
            coinAmmount = numString(cents.substring(0, 2).toCharArray());
        }
        if(inLength==1){ //if only one coin exists, generate words
            coinAmmount = numbers(cents.charAt(0),nums);
        }
        if(!coinAmmount.isEmpty() && !coinAmmount.matches("")){ //if there were any coins, add coin currency too
            coinAmmount = coinAmmount+" "+coin(currency);
        }
        if(!billAmmount.isEmpty() && !billAmmount.matches("") && 
           !coinAmmount.isEmpty() && !coinAmmount.matches("")){ //if there were coins and bills, add 'and'
            billAmmount+=" and";
        }
        return (billAmmount+" "+coinAmmount).trim();
    }
    
    private static String numString(char[] in){
        int lastZero=-1;
        String ammount="";
        for(int i=0;i<=in.length-1;i++){
            if((i%3)==0 && in.length-1>i && in[i+1]=='1'){
                ammount = numLevel(i, in[i], true) + ammount;
                i++;
            }else{
                while(i<in.length-1 && in[i]=='0'){
                    lastZero=i;
                    i++;
                }
                if(i<=in.length){
                    if(lastZero!=-1 && (i%3)!=0){
                        ammount = level(lastZero-(lastZero%3)) + ammount;
                        lastZero=-1;
                    }
                    ammount = numLevel(i, in[i], false) + ammount;
                }
            }                    
        }
        return ammount.trim();
    }
    
    private static String numLevel(int i, char c, boolean isTeen){
        switch(i%3){
            case 0:
                if(isTeen){
                    return numbers(c,teens)+level(i);
                }else{
                    return numbers(c,nums)+level(i);
                }
            case 1: return numbers(c,tens);
            case 2: if(c!='0'){
                        return numbers(c,nums)+" hudred";
                    }else{
                        return numbers(c,nums);
                    }
        }
        return "";
    }
    
    private static String bill(int i){
        if(billPlural){
            return bills[i];
        }
        return bill[i];
    }
    
    private static String coin(int i){
        if(coinPlural){
            return coins[i];
        }
        return coin[i];
    }
    
    private static String level(int i){
        if(i%3==0){
            return " "+levels[(i/3)];
        }
        return "";
    }
    
    private static String numbers(char i, String[] words){
        return " "+words[Integer.parseInt(i+"")];
    }
    
    private static String removeLeadingZeros(String in){
        while(in.length()>0 && in.charAt(0)=='0'){
            in=in.substring(1);
        }
        return in;
    }
    
    private static String reverse(String s) {
        if (s.length() <= 1) { 
            return s;
        }
        return reverse(s.substring(1, s.length())) + s.charAt(0);
    }
    
}
